#include "Header.h"

#include <iostream>
#include <thread>
#include <vector>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <algorithm>
#include <random>
#include <iomanip>


// ��������������� ������� ��� ��������� ��������� ����� �� ���������
int getRand(const int min, const int max)
{
    static std::default_random_engine gen(static_cast<unsigned>(std::chrono::system_clock::now().time_since_epoch().count()));
    std::uniform_int_distribution<int> dis(min, max);

    return dis(gen);
}

// ��������� ������, ������� ������ ���������
struct Student
{
    std::string name;
    int age;
    float averageGrade;

    Student(std::string n, int a, float g) : name(n), age(a), averageGrade(g) {}
};


// ������� ����������
void produce(std::vector<Student>& students, std::mutex& mtx, std::condition_variable& cv)
{
    while (true)
    {
        // �������� ��������� ��� �� �������
        std::string names[] = { "Alice", "Bob", "Charlie", "David", "Eve" };
        std::string name = names[getRand(0, 4)];
        name.resize(9, ' ');    // ������� ������ ������ �� 9, �������� ���������, ��� ��������� ����������� � �������

        // �������� ��������� ����� ��� �������� � ������
        int age = getRand(18, 38);
        float averageGrade = getRand(50, 100) / 10.0; // ����� �� 5.0 �� 10.0

        // ������ ���������� �������� ��� ������� ��������� - ������� ������������� �������������
        std::unique_lock<std::mutex> lock(mtx);

        // ��������������� � ��������, ����� ��� ���� �������
        students.push_back(Student(name, age, averageGrade));

        lock.unlock();      // ������������ �������
        cv.notify_one();    // ��������� �� �������� ����������

        // ��������� ��������
        std::this_thread::sleep_for(std::chrono::milliseconds(getRand(1000, 1500)));
    }
}

void consume(std::vector<Student>& students, std::mutex& mtx, std::condition_variable& cv, bool& isFull)
{
    while (true)
    {
        // ������ ���������� �������� ��� ������� ��������� - ������� ������������� �������������
        std::unique_lock<std::mutex> lock(mtx);
        // ������������ ������� ������� ���� �� ������� ���������� �������
        cv.wait(lock, [&students] { return !students.empty(); });
        // ����� ������� ��������� ������� �����������

        // ��������� ������ � ������� ������-�������
        // ������ ����� ��� ���� �������, ������� ��������������� � ��� ��� �����������
        std::sort(students.begin(), students.end(), [](const Student& a, const Student& b)
            {
                return a.averageGrade > b.averageGrade;
            });

        system("cls");  // ������� �������

        // ����� �������
        std::cout << "   \|   Name   | Age \| AverageGrade " << std::endl;

        // ����� ����� �������
        // �� ��� ���������� ������ ����� ��� ���� �������. ������� �� ��� ��� �����������,
        // ����� �� ������� �� ����� ����������������� ������, ������� ������ ��� ����������
        for (int i = 0; i < students.size(); i++)
        {
            std::cout << std::setw(2) << i + 1 << " \| " << students[i].name << "\| " << students[i].age << "  \| " << students[i].averageGrade << std::endl;
        }
        
        // �������� ������� ���������� �� ������� ������ ��� ���� ������� �������, �������
        // �� ��� ��� �����������
        if (students.size() >= 15)
        {
            // ������������ ������� ����� ������ ��� ������ ��� break, 
            // � �������� ��������� lock.unlock() ����� ����� ����� ��������
            lock.unlock();  

            isFull = true;
            cv.notify_one();    // ��������� �� �������� ����������
            break;
        }

        // ������������ �������, ���� ������� ����� �� ����������
        lock.unlock();  

        std::this_thread::sleep_for(std::chrono::milliseconds(500)); // �������� �� ����������     
    }
}

int main_table()
{
    srand(static_cast<unsigned int>(time(0)));
    rand();

    // �������� ����� ��� ���� ������� ����������
    std::vector<Student> students;
    std::mutex mtx_1;   // �.�. � ���������� ����� �������� ������ � ����� �������, ����������� ������ � ��������
    std::condition_variable cv;
    bool isFull = false;

    // ������ ��� ������ ���������� � ���� ����� �����������
    std::thread producer_thread1(produce, std::ref(students), std::ref(mtx_1), std::ref(cv));
    std::thread producer_thread2(produce, std::ref(students), std::ref(mtx_1), std::ref(cv));
    std::thread consumer_thread(consume, std::ref(students), std::ref(mtx_1), std::ref(cv), std::ref(isFull));

    // ������ ������������ ����� �������� ����������
    // ��� �� ����� ���������� ��������� �� ������
    producer_thread1.detach();
    producer_thread2.detach();

    // ������ ���������� �������� ��� ������� ��������� - ������� ������������� �������������
    std::unique_lock<std::mutex> lock(mtx_1);
    // ������������ ������� ������� ���� �� ������� ���������� �������
    cv.wait(lock, [&] { return isFull; });

    // ���������� ���������� ������ �����������
    consumer_thread.join();

    return 0;
}