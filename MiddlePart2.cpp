#include "Header.h"

#include <iostream>


int main() 
{
    std::cout << "Enter 1 to start table programm\n"
              << "Enter 2 to start game (Please switch to EN keyboard layout)\n";
    int input;
    std::cin >> input;
    std::cin.clear(); // �� ������, ���� ���������� ���� ���������� � �������
    std::cin.ignore(32767, '\n');

    switch (input)
    {
    case 1:
        main_table();
        break;
    case 2:
        main_game();
    default:
        break;
    }

    return 0;
}