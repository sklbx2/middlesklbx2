#include "Header.h"

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <conio.h>
#include <deque>

// ������������ ��� ����������� ������
enum ELane
{
    LEFT,
    RIGHT
};

// ������ �������� ����

const int Width = 13;
const int Height = 10;
char game_field[Height][Width];

// ����� ����������� �� ������� ����
class Obstacle
{
public:
    int x, y;

    Obstacle(int startX, int startY) : x(startX), y(startY) {}

    // ��� ���������� ����������� ��������� �� �������� ���� ����
    // � ��������������, ���� �� ��� ������ � ������� �������� ����
    void update()
    {
        y++;

        if (y < Height)
        {
            game_field[y][x] = '#';
        } 
    }
};

// ������ �����������

const int MaxObstacles = 5;
std::deque<Obstacle> obstacles;


// ����� ������ ��� ����

ELane playerLane = LEFT;

bool isGameOver = false;


// ������ ��� ��������������

std::mutex mtx;
std::condition_variable cv;
bool bInput = false;

// ���������� ����������� ����� ������ � ���������� �������� (�����-����)
void FillRoad(int position)
{
    for (int i = 0; i < Height; i++)
    {
        // ������������ ������ ������ ������ ������ (��� ����)
        for (int j = 1; j < Width-1; j++)
        {
            game_field[i][j] = ' ';
        }
    }

    //for (int i = 0; i < Height; i++)
    //{
    //    // ������ ����� �� �������� ������ ����� ���� �������
    //    if (i % 3 == position)
    //    {
    //        game_field[i][6] = '\|';
    //    }
    //}

    for (int i = position; i < Height; i+=3)
    {
        // ������ ����� �� �������� ������ ����� ���� �������
        if (i % 3 == position)
        {
            game_field[i][6] = '\|';
        }
    }
}

// ���������� ������ ������
// �.�. ������� �������� ���������, ��������� �� ���� ���
void InitialFillRoad()
{
    // ����������� ��������������� ������ �� ���� ������ �� ������� ������
    for (int i = 0; i < Height; i++)
    {
        game_field[i][0] = '\|';
        game_field[i][Width - 1] = '\|';
    }
}

// �������� ����������� � ��������� ������ ������
void GenerateObstacle()
{
    // ���������� ��������, ����� ����������� �� ���������� ����� ���� �� ������
    static int delay = 0;

    // ���������� ����������� � ����������� ������, � ���� ������ ��������
    if (rand() % 3 == 0 && delay <= 0)
    {
        // ������������� �������� �� ���������� �� ������������ ���������� ���������� �� �����
        if (obstacles.size() < MaxObstacles)
        {
            // �������� �������� ������
            ELane lane = static_cast<ELane>(rand() % 2);

            // �������� �������� ����� ��� ������ ������ � ������ (��� ����������� ������������)
            // 3 � 8 - ������� ����� ����� � ����� � ������ ������� ��������������
            int x = (static_cast<bool>(lane) ? 3 : 8) + rand() % 2;

            // ��������� ����������� � ������� � ��������� ����������� �� ������, ����� ��� ������
            // ������ update, ����������� ������� �� ������ �������
            obstacles.push_back(Obstacle(x, -1));
        }

        // ������������� ��������
        delay = 2;
    }

    delay--;
}

// ����� ������ �� ����� ������
void MovePlayer(ELane newLane)
{
    // � ����� �� ������������ �������� � ������ ����� �� �������
    // ������ ��� � ���� ��������� ����������� _kbhit()
    //bInput = true;
    {
       //std::unique_lock<std::mutex> lock(mtx);

        switch (newLane)
        {
        case LEFT:
            if (playerLane != LEFT)
            {
                playerLane = LEFT;
            }
            break;
        case RIGHT:
            if (playerLane != RIGHT)
            {
                playerLane = RIGHT;
            }
            break;
        default:
            break;
        }
    }
    //bInput = false;
    //cv.notify_one();
}

void Update()
{
    static int position;
    {
        std::unique_lock<std::mutex> lock(mtx);
        //cv.wait(lock, [] {return !bInput; });

        // ��������� ������
        FillRoad(position);
        position++;   // ������� ������ ����
        if (position >= 3)
        {
            position = 0;
        }

        // ���������� ������
        switch (playerLane)
        {
        case LEFT:
            game_field[Height - 1][3] = 'O';
            game_field[Height - 1][4] = 'O';
            break;
        case RIGHT:
            game_field[Height - 1][8] = 'O';
            game_field[Height - 1][9] = 'O';
            break;
        default:
            break;
        }

        // ���������� ����������� (�������� ����)
        for (auto& o : obstacles)
        {
            o.update();

            // ���� ����������� ����� �� ������� ����� - ������� ���
            if (o.y > Height)
            {
                obstacles.pop_front();
            }
        }

        // ��������
        // ���� ����� ���������� �����������, �� ����� ������ ���������� �����������,
        // �� ����� ��������
        switch (playerLane)
        {
        case LEFT:
            if (game_field[Height - 1][3] == '#'
                || game_field[Height - 1][4] == '#')
            {
                isGameOver = true;
            }
            break;
        case RIGHT:
            if (game_field[Height - 1][8] == '#'
                || game_field[Height - 1][9] == '#')
            {
                isGameOver = true;
            }
            break;
        default:
            break;
        }
       
        
        // ��������� ����� �����������
        GenerateObstacle();
    }
}

// �������� �������� �������� ����
void Render()
{
    std::unique_lock<std::mutex> lock(mtx);
    system("cls");
    for (int i = 0; i < Height; i++)
    {
        for (int j = 0; j < Width; j++)
        {
            std::cout << game_field[i][j];
        }
        std::cout << std::endl;
    }
}

// �������� ����� �� �������
void InputThread()
{
    while (!isGameOver)
    {
        if (_kbhit())
        {
            char key = _getch();
            key = tolower(key);
            if (key == 'a')
            {
                MovePlayer(LEFT);
            }
            else if (key == 'd')
            {
                MovePlayer(RIGHT);
            }
        }
    }
}

void RenderThread()
{
    while (!isGameOver)
    {
        Render();
        std::this_thread::sleep_for(std::chrono::milliseconds(25)); // �������� ������� ���������� ������ �� �������������
    }

    // ��������� ��������, ��� ������� ���� �����������
    Render();
}

void GameLoop()
{
    while (!isGameOver)
    {
        Update();
        std::this_thread::sleep_for(std::chrono::milliseconds(100)); // ��������� ����� ����� �������
    }
}

int main_game()
{
    srand(static_cast<unsigned int>(time(0)));
    rand();

    InitialFillRoad();
    FillRoad(0);

    std::thread inputThread(InputThread);
    std::thread renderThread(RenderThread);
    std::thread gameThread(GameLoop);

    inputThread.join();
    gameThread.join();
    renderThread.join();

    std::cout << std::endl << "  GAME OVER!\n\n Press Enter to exit" << std::endl;

    // ���������� ����� �� ������ ��� ������ �� ����
    char input[256];
    std::cin.getline(input, 256);

    return 0;
}